$('.slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	dots: true,
	adaptiveHeight: true
});
$('.slider').slickLightbox({
	src: 'src',
	itemSelector: 'div img'
});