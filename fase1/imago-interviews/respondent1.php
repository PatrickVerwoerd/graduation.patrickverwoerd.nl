<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 1</li>
			</ul>
		</section>
	</header>
	<section>
		<h1>Imago interview respondent 1</h1>

		<h3>Waar aan denk je bij VSGM?</h3>

		<p>Dan denk ik in eerste instantie aan <code>een drukker</code>. Dat komt door de afkomst. Ze zitten nu in een omslag waarbij ik zie dat ze moeite hebben om keuzes te maken. <code>Het feit dat het beeld van buitenwereld nog niet is wat ze zelf willen komt ook deels door hun eigen twijfel</code>. Een klant loopt altijd drie jaar achter, dus als jij een product hebt snapt de klant pas 3 jaar later wat jij doet. En dat zorgt voor problemen nu. </p>

		<p><code>Het is een drukker die er websites bij doet</code>. </p>

		<h3>Welke diensten biedt VSGM aan?</h3>

		<p><code>Van opmaak tot drukwerk tot websites tot artwork</code> tot eigenlijk kan je het zo gek niet verzinnen. Als ik ze vraag om een beurs stand of bedrukte tafelbladen hebben ze daar ook nog een oplossing voor. Daarbij is het natuurlijk wel zo dat ze dat niet zelf doen maar dat ze dat inkopen. Dat siert ze enerzijds, aan de andere kant is het voor de klant die ze niet kent <code>moeilijk om een beeld te vormen wat het nu eigenlijk voor een bedrijf is</code>. Keuzes maken is enorm belangrijk in deze tijd en VSGM is daar niet goed in. Dat is de grootste uitdaging voor ze denk ik.</p>

		<h3>Van welke maak jij gebruik?</h3>

		<p>Ik maak gebruik van de diensten voor mijn opdrachtgevers en daarbij gaat het om ontwerp, drukwerk, logo ontwerp en artwork. Het ruime scala eigenlijk wel. </p>

		<h3>Wat heeft je doen besluiten om die diensten door VSGM uit te laten voeren en niet door anderen?</h3>

		<p>Dat is gebaseerd op een aantal parameters voor mij. De mensen <code>met wie ik moet werken vind ik heel belangrijk en die klik heb ik</code>. Ook vind ik het erg prettig dat ik <code>direct contact</code> kan hebben met de mensen die het werk uitvoeren. <code>Met Carolien heb ik ook een enorme klik, die weet precies mijn woorden visueel te vertalen</code>. De <code>korte lijntjes en de goede contacten</code>, dat vind ik belangrijker dan te weten welke drukpers er in de drukkerij staat. Dat vind ik veel minder interessant.</p>

		<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

		<p>Dan vertel ik ze dat het <code>prettige mensen zijn om mee te werken met direct contact</code>. Ik en mijn opdrachtgevers vinden Van Schaik wel <code>duur, ze zitten echt bovenin de markt</code>. Aan de andere kant kun je dat ook alleen maar <code>permitteren als je kwaliteit levert en dat doen ze</code>. Ik heb wel eens meegemaakt dat het door Van Schaik werd afgekeurd terwijl ik dacht ‘waarom is dit in hemelsnaam afgekeurd’. Maar dan wilde hij het ook zo verdomde goed hebben dat hij zei ‘dit gaat bij mij de deur niet uit’. Daar zie je ook de roots, dat heeft <code>Van Schaik veel meer met drukwerk dan met websites</code>. Daar heeft hij (Fred) minder kijk op, minder gevoel bij. Commercieel weet hij het uitermate succesvol in de markt te zetten maar <code>er mist wel een scherphouder die op dat terrein zegt ‘dit gaat de deur niet uit, dit is niet goed genoeg’</code>.</p>

		<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

		<p>Het eerste wat ik zou doen is <code>‘grafi’ uit mijn naam schrappen</code>, ik zou er media van maken. En ik zou echt moedwillig keuzes gaan maken; dit doen we wel en dit doen we niet. En dan keuzes maken waarbij we als Van Schaik echt kunnen zeggen ‘hier zijn we gewoon rete goed in’. En ik zou mijn aanbod in de markt omdraaien, <code>ik zou een digitaal georiënteerd bedrijf in de markt zetten dat óók analoge dingen doet</code> i.p.v. een analoog bedrijf dat óók digitale dingen doet. Het drukwerk als bijzaak zien, en dat gaat hartstikke pijn doen, ook in de omzet. <code>Van Schaik moet door maar houdt nog steeds vast aan hun afkomst en dat moeten ze niet langer uitstellen</code>. Het gaat een hoop geld kosten om daar voor te kiezen maar in deze comfortzone gaan ze nooit vernieuwen. Draai die drukkerij is een jaar op slot, pas dan ga je vol voor waar je voor wilt staan. Je wordt pas ergens goed in als je er vol voor kiest. Zolang je blijft hangen in drukwerk wordt je nooit extreem goed in websites want de urgentie om uit te blinken is niet groot genoeg, X% van de omzet haal je toch uit drukwerk. Dus waarom zou je daar in investeren, waarom zou je een veer laten, waarom zou je mensen meer inspireren om het uiterste er uit te halen in de wetenschap dat de kachel toch brand? Als de kachel brandt ga je niet harder lopen, de kachel moet uit.</p>

		<h3>Heb je wel eens negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

		<p>Nee, behalve het dispuut over het boek (Oudewater750). Het is een gave van Fred om in zijn zakelijkheid hele goede deals te maken. Ik kan het niet staven met bewijs maar ik denk dat als je 20 drukkers vraagt ‘wat is je netto resultaat’ dat VSGM er moet kop en schouders bovenuit steekt. Dus dat doen ze hartstikke goed maar af en toe gaat hij daar wat te ver in. <code>Als ik voor een opdrachtgever offertes opvraag is VSGM soms 25% duurder, dat is veel te veel. Ze zijn dan wel de Rolls Roys onder de drukkers maar 25% is veel te veel, 5% kan ik nog uitleggen.</code></p>

		<p><code>Er wordt weinig vastgelegd, ik probeer het vast te leggen in de mail met een lijstje o.i.d.. Maar als ik als opdrachtgever dat niet doet dan gebeurt dat ook helemaal niet. Een werkverslag van het proces moet ik voor mijn klanten doen maar VSGM zie ik niets. Ik denk dan ‘hoop dat het allemaal maar goed gaat’.</code> </p>

		<p>Ik werk ook met <code>andere webbouwers, die over het algemene meer complexe websites maken. Die hebben een PDCA dossier (Plan, Do, Check, Act), die richten vanaf moment 1 een SLA in op basis waarvan ze komen tot een bepaalde oplossing voor de websites, die hebben scrumsessies waar je als klant aan deelneemt.</code> Als je dat ondergaat dan ben je echt deelgenoot aan het proces, ze luisteren naar me. Sterker nog, ze hebben het opgeschreven en afgevinkt, <code>dan krijg je het idee dat ze echt alles onder controle hebben.</code> Als VSGM het bedrijf op zijn kop durft te zetten dan ben je binnen een jaar ook aan het scrummen omdat je gewoon niet meer anders kan. <code>Dan zou het bedrijf kwadratisch beter worden, nu doet VSGM dingen die het al kan en daar leer je niets van.</code> Mike zou daar karakterologische beter bij passen dan Fred denk ik.</p>

		<h3>Wat zou je overhalen om vaker voor VSGM te kiezen?</h3>

		<p>Als VSGM een <code>professionaliseringsslag gaat maken in haar procesbegeleiding.</code> Ik ga nu soms moedwillig met een opdrachtgever niet naar VSGM omdat ik denk ‘dat gaat de klant niet trekken, op basis van welk kwaliteitsstandaard ga we nu aan het werk? Op goed vertrouwen? Dan gaan we wel naar een ander’. <code>En die professionaliseringsslag gaat VSGM pas maken als ze er afhankelijk van zijn.</code> Zolang ze dat niet zijn is het een extra geld molentje dat draait maar op de lange termijn is dat geen goede ontwikkeling.<code> VSGM is nu op het niveau dat iedereen dingen doet die ze al kunnen maar niets verbeterd dus er moet nu worden ingegrepen.</code> Stel je er dan maar op in dat je winst 0 is en VSGM moet dan bedenken ‘wil ik dat, heb ik dat er voor over?’. Als ze dat niet willen dan moet er niet aan begonnen worden maar dan kabel je voort en komen ze links en rechts voorbij tot je te laat bent. Je moet het dak repareren als de zon schijnt. </p>

		<h3>Wat vind je van de huidige visuele uitstraling?</h3>

		<p>Wat VSGM goed doet is ‘practise what you preach”, het is <code>zonder meer bijzonder drukwerk.</code> Je kan zien dat het afwijkt van het reguliere wat je ziet en dat is goed. <code>Maar van ‘Communicatie met visie’ krijg ik echt jeuk, 5 blokjes met 1 blokje waar niets in staat (websites 2e), verschrikkelijk.</code> Kom op, daar is niet goed genoeg over nagedacht, dat had optisch anders opgelost moeten worden! ‘Communicatie met visie’ doet echt niets op map, hou het weg want welke visie? Welke visie hebben jullie dan op communicatie, die hebben jullie helemaal niet. Kom met iets wat authentiek is of hou het weg. Ik zie soms kretologie voorbij komen waarbij ik denk ‘phoe phoe rustig aan’. <code>VSGM moet veel beter in staat zijn om datgene wat ze uniek maakt onder het voetlicht te krijgen</code> en dat is jouw uitdaging want hoe gaan ze dat dan doen. <code>Je moet echt op zoek gaan naar unieke toegevoegde waarden die ze leveren en dat moet gaan vertellen... Ze moeten gaan investeren in zichzelf, in hun kennis, in hun ervaring, in hun mensen, als ze dat toch durven lopen ze niet meer achter de feiten aan.</code> VSGM is innovatief geen voorloper. Je hoeft niet helemaal vooraan te lopen maar je moet wel bedenken ‘wil ik in de kopgroep zitten, in het peloton of zelfs in de bus? Het busje is de minste inspanning, het peloton is anoniem en daarbij ben je een van de velen en in de kopgroep win je af en toe en af en toe vlieg je de bocht uit. </p>
	</section>
	<?php include"../../includes/footer.php" ?>
</body>
</html>