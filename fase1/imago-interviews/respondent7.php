<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 7</li>
			</ul>
		</section>
	</header>
	<section>
<h1>Imago interview respondent 7</h1>

<h3>Waar aan denk je bij VSGM?</h3>

<p><code>Aan een drukkerij die dingen er bij doet.</code></p>

<h3>Welke diensten biedt VSGM aan?</h3>

<p>Concept en ontwerp, websites en de drukkerij </p>

<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

<p><code>Ik zou niet zeggen dat ik bij een ontwerpstudio werk; er zit een drukkerij bij, ik ontwerp daar dingen</code> en leer daar dus veel over drukwerk en over de programma’s maar niet heel veel over ontwerpen.</p>

<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

<p><code>Sowieso dat lelijke logo weg; sorry maar die is echt lelijk. Het is rommelig, niet duidelijk wat jullie nu echt doen, het is te veel... Als jullie zelf ontwerpen moet je ook een goed logo hebben toch? Je moet het goede voorbeeld geven. Dat als eerste, vervolgens het interieur ook aanpakken want als je hier binnen komt is het een oude boel. De huisstijl en de hele uitstraling van het bedrijf zou ik aanpakken.</code> Dan komen de andere (leukere) opdrachten vanzelf, als je het goede voorbeeld geeft. Al weet ik niet hoe jullie nu naar buiten treden, <code>ik bijvoorbeeld had nog nooit van jullie gehoord terwijl ik dichtbij woon.</code></p>

<h3>Heb je wel eens positief of negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

<p>Nooit negatief. Positief wel; super leuke stage en gezellig. Het is echt een goed bedrijf vind ik, <code>jullie werken goed samen maar dat komt niet goed tot uiting</code>. Het wordt bij jullie altijd wel wat de bedoeling is wat het moet worden. </p>

<h3>Wat is kenmerkend voor VSGM?</h3>

<p>Dat weet ik niet zo hoor, het lelijke logo? Verder heb ik niet echt iets kenmerkends ofzo. Ik denk aan zoveel dat ik niet weet waar ik aan moet denken, jullie doen gewoon veel te veel. <code>Met die blokjes op jullie huisstijl die niemand leest, die schetsjes erbij met tekstjes die het leuker maken maar niet duidelijker.</code> Ik weet het niet.</p>

<h3>Wat vind je van de huidige fysieke uitstraling en dan met name de website?</h3>

<p><code>Ik vind het niet professioneel overkomen</code>. Het foliedrukwerk op het briefpapier is erg mooi maar <code>die blokjes en de letterspaghetti vind ik maar niets. Die letterstpaghetti vind ik meer iets voor in een basisschoolboek</code>, niet als huisstijl element. <code>Van de map is de techniek van het drukwerk mooi maar de vormgeving is lelijk.</code> Bij de tekst ‘communicatie met visie’ heb ik helemaal geen gevoel, weet niet echt wat ze daar nu mee bedoelen. <code>De website is ook onduidelijk, hoe moet ik nu komen bij wat jullie doen? Ik heb toevallig laatst pas ontdekt dat je op de blokjes in de footer kan klikken, dat valt helemaal niet op. Jullie blog is ook veel te leeg en de laatste tweet is alweer een jaar oud.</code></p>

<p>Ik ben wel trots dat ik stage kan lopen omdat jullie allemaal zo goed weten wat jullie doen. <code>Jullie hebben heel veel kennis maar dat komt niet tot uiting bij jullie. Het komt niet over alsof jullie heel professioneel zijn terwijl jullie dat wel echt zijn.</code></p>

	<?php include"../../includes/footer.php" ?>
</body>
</html>

