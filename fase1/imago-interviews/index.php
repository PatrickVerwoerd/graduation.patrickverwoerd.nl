<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<title>Graduation appendix || Patrick Verwoerd</title>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/graduation/fase1/">Fase 1</a></li>
				<li>Imago interviews</li>
			</ul>
		</section>
	</header>
	<section>
		<div class="contents">
			<h2>Fase 1</h2>
			<h3>1.1 Transcripten imago interviews</h3>
			<ul>
				<li><a href="respondent1.php">1.1.1 Transscript respondent 1</a></li>
				<li><a href="respondent2.php">1.1.2 Transscript respondent 2</a></li>
				<li><a href="respondent3.php">1.1.3 Transscript respondent 3</a></li>
				<li><a href="respondent4.php">1.1.4 Transscript respondent 4</a></li>
				<li><a href="respondent5.php">1.1.5 Transscript respondent 5</a></li>
				<li><a href="respondent6.php">1.1.6 Transscript respondent 6</a></li>
				<li><a href="respondent7.php">1.1.7 Transscript respondent 7</a></li>
			</ul>
		</div>
	</section>
	<?php include"../../includes/footer.php" ?>
</body>
</html>