<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 5</li>
			</ul>
		</section>
	</header>
	<section>
<h1>Imago interview respondent 5</h1>

<h3>Waar aan denk je bij VSGM?</h3>

<p>In eerste instantie aan Fred. De zakelijke relatie met hem gaat echt al jaren terug, ik denk al 10 jaar inmiddels. <code>Verder gaat mijn eerste gedachte uit naar drukwerk en de verspreiding daarvan</code> omdat Fred dat in het verleden heel veel voor ons gedaan heeft.<code> Maar tegenwoordig is dat ook websites</code>; dat is wel meer iets van de laatste tijd omdat we nu natuurlijk met jullie werkzaamheden aan het doen zijn nu. Fred begon twee jaar geleden ofzo met ‘ja we doen ook websites bla bla’ en inmiddels heeft hij dat bewezen ook te doen voor ons met o.a. de sms actie.</p>

<p>Wij hanteren natuurlijk een concept waarbij we een machine leveren met daarbij een heel marketingconcept voor onze klanten. Vroeger zat daar een heel verspreidingspakket bij met ik zo’n 50.000 mailing kaarten per klant en dat maal een paar 100 klanten dan weet je wel dat Fred een leuke klant aan ons had. Zo is dat toen begonnen, daarna een tijd niet meer en tegenwoordig is dat weer opgepakt omdat we een nieuw concept gingen aanpakken waar Fred mooi op in wist te spelen.</p>

<h3>Welke diensten biedt VSGM aan?</h3>

<p>Fred probeert me altijd alles aan te bieden (hahaha). <code>Maar jullie werkzaamheden bestaan voornamelijk uit drukwerk in alle soorten, webdesign en zelfs hosting en ontwikkeling volgens mij, design in de meest brede zin van het woord dus artwork, dtp etc. voor zowel online als offline. En jullie hebben natuurlijk nog dat Pinca systeem.</code></p>

<h3>Van welke maak jij gebruik?</h3>

<p><code>Het belangrijkste wat wij nu bij jullie afnemen is een totaal pakket en ontzorging. Dat is ook cruciaal voor jullie werkzaamheden, jullie nemen het uit handen.</code> Jullie zijn gewoon meer dan een drukkerij, jullie kunnen design verzorgen, jullie sms dienst, email campagnes. Voor een partner zorgen jullie voor totale ontzorging op het gebied van marketing en communicatie en dat is – in ieder geval voor ons – enorm handig want dan hebben wij daar geen omkijken meer naar. </p>

<h3>Wat heeft je doen besluiten om die diensten door VSGM uit te laten voeren en niet door anderen?</h3>

<p><code>Dat jullie ons zo ontzorgen dat wij er geen omkijken meer naar hebben is de reden dat we het door jullie laten uitvoeren.</code> Ook gezien de relatie met elkaar; het vertrouwen is er en de resultaten uit het verleden zijn goed. </p>

<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

<p><code>Als de partner die je ontzorgt op marketing en communicatie gebied.</code> Dan vragen ze wellicht wel ‘wat kost het dan’ en dan zeg ik eerlijk <code>‘het is zeker niet de goedkoopste maar je krijgt wel kwaliteit in product en dienstverlening’</code>. Tegen een internetdrukker kunnen jullie natuurlijk sowieso niet op maar ook voor een ‘gewone’ drukker zitten jullie goed aan de prijs.</p>

<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

<p>Het is goed zoals jullie het de laatste 10 jaar hebben aangepakt; drukkerijen hebben natuurlijk flink op hun kloten gekregen en jullie hebben je daar mooi doorheen weten te slaan. </p>

<h3>Heb je wel eens positief of negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

<p>Niet negatief, we zijn wel ooit bij Van Schaik weggegaan maar ook toen hebben we er niet negatief over gesproken. Toen we jaren geleden de mailingkaarten deden werden <code>jullie gewoon echt te duur</code>. Toen was er er een drukkerij die exact hetzelfde leverde voor veel minder geld. </p>

<p><code>Positief zeker wel, dan gaat het altijd over het ontzorgen wat jullie doen.</code></p>

<h3>Wat zou je overhalen om vaker voor VSGM te kiezen?</h3>

<p><code>De prijs uiteraard maar die is te rechtvaardigen</code>. Jullie doen eigenlijk altijd alles al voor ons behalve soms wat drukwerk wat we bij internetdrukkers onderbrengen. <code>We hebben één huisstijl dus we gaan ook zeker niet met meerdere partijen werken, dat schiet ook niet op.</code> Of het gaat goed en we laten jullie alles doen of jullie verpesten het en dan gaat alles weg, zo simpel is het. </p>

<h3>Wat is kenmerkend voor VSGM?</h3>

<p><code>Jullie huisstijl welke ik wel grappig vind. Die is een beetje art-deco, wat op zich niet heel gebruikelijk is en wat het soms ook wel onoverzichtelijk maakt.</code> De achterkant van jullie briefpapier bijvoorbeeld, daar wordt mijn collega helemaal knettergek van. Dan wordt het helemaal wazig voor zijn ogen zegt hij. </p>

<h3>Wat vind je van de huidige fysieke uitstraling en dan met name de website?</h3>

<p>Hij is zoals gezegd grappig. Ik had het zeker niet zo gedaan.<code> De website zelf zou ik anders doen persoonlijk, die is niet heel overzichtelijk en duidelijk</code> Je moet goed kijken om de structuur te vinden. <code>De map is wel grappig, daarmee toon je aan dat je kwaliteitsdrukwerk levert. Maar de site is – ook voor jullie – cruciaal, daarmee verkoop je je diensten en referenties wat de bezoeker zou moeten overtuigen. Wat heb je gemaakt, wat bied je aan, etc. Dat moet er opstaan.</code> </p>

<p>Er staat ook ergens op jullie website dat jullie iets van ‘A tot Z doen’ maar leg dan ook uit wat het is, dat is voor jullie misschien vanzelfsprekend maar voor anderen niet. Dat zegt niemand iets joh.</p>

<p><code>Het sluit wel een beetje aan bij dat wat jullie doen. Het is onderscheidend en dat zijn jullie ook wel.</code> Fred gooit nog wel eens wat dingen naar mij toe waarbij ik denk ‘ja dat is wel echt weer wat anders, zoals die sms actie’ dat meedenken is mooi. <code>Maar visueel gezien is het allemaal niet zo mooi.</code></p>

	<?php include"../../includes/footer.php" ?>
</body>
</html>
