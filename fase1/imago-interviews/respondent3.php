<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 3</li>
			</ul>
		</section>
	</header>
	<section>
<h1>Imago interview respondent 3</h1>

<h3>Waar aan denk je bij VSGM?</h3>

<p>Ik denk sowieso aan ‘Van Schaik en niet aan VSGM’. Maar <code>ik denk aan de drukkerij</code> waar wij mee samen werken. Ik kies voor hen vanwege de locatie, als we iets in de regio kunnen krijgen heeft dat onze voorkeur. </p>

<h3>Welke diensten biedt VSGM aan?</h3>

<p>Volgens mij alles, van concept tot website tot design tot drukwerk. <code>Zoals ik naar jullie kijk zijn jullie een platform waar je terecht kunt voor alles m.b.t. visuele communicatie binnen je bedrijf.</code> <code>Ik was verbaast over de omvang van jullie studio, dat die zo groot is en dat ze echt in dienst zijn.</code> Ik begrijp dat jullie het op die manier aanpakken, drukwerk is een lastige markt vanwege de overcapaciteit dus dan worden we een platform. <code>Goede keuze denk ik wel maar of jullie dat waar kunnen maken weet ik niet.</code> Op die manier heb ik niet met jullie samengewerkt maar <code>gezien jullie visuele uitstraling maken jullie het niet waar.</code> Hebben jullie een art-director of kopen jullie dat in? Je grafisch vormgever is geen art-director hè, die kan geen concepten. <code>Jullie blijven gewoon een drukkerij, en best een mooie drukkerij ook.</code></p>

<p><code>Jullie focus is gericht op jullie eigen productie. Je zou bijna zeggen “gooi die drukpers er uit”, ik zie jullie als een communicatieplatform. Als een mediaontwikkelplatform. Jullie zijn een platform waar je in bepaalde media kan voorzien omdat jullie dat maken en dat is goed. Ik zie jullie als een drukkerij+</code></p>

<h3>Van welke maak jij gebruik?</h3>

<p>Specialistisch drukwerk</p>

<h3>Wat heeft je doen besluiten om die diensten door VSGM uit te laten voeren en niet door anderen?</h3>

<p><code>VSGM staat bekend als een goede drukkerij en gespecialiseerd in speciaaldrukwerk.</code> Wat wij daarbij belangrijk vinden is dat er iemand kijkt naar het hele proces en dat houdt ook in dat je kijkt naar hetgeen is aangeleverd. <code>Onze ervaring is dat dat zo werkt bij jullie en dat is erg prettig want wij maken ook fouten en dan is VSGM een mooi vangnet.</code> Wij werken ook met prijsvechters, voor visitekaartjes bijvoorbeeld. Dat ziet er prima uit maar <code>als echte kwaliteit belangrijk is dan komen we bij VSGM terecht, want die zien we echt als speciaal drukkerij.</code> <code>Jullie lullen al heel snel in je eigen straatje en of dat het beste is voor de klant...?</code></p>

<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

<p><code>Als een speciaaldrukkerij uit Oudewater die drukt op Heidelberg. Dat zie ik als enorme meerwaarde, dat is de Rolls Roys onder de drukpersen</code> en die koop je niet zomaar. Rondom de persen is het allemaal strak en schoon, goed verlicht en opgeruimd. Dat is belangrijk want daarin herken je de werkwijze. </p>

<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

<p>Daar ken ik jullie niet goed genoeg voor, dat is heel moeilijk om antwoord op te geven. Ik kijk heel anders naar een bedrijf dan dat Mike en Fred dat doen. Dat zijn twee personen die een enorme stempel drukken op het bedrijf en dan lopen er nog wat kroonprinsen rond en die hebben dat min of meer overgenomen maar die hebben er ook nog een eigen mening in. En samen vormen ze, met de historie, de ziel van het bedrijf. Dan kan ik wel zeggen ‘ik zou het zo doen’ maar dat is niet de ziel. Je kan alleen denken ‘hoe kan ik dat zo mooi mogelijk aan de buitenwereld laten zien’. <code>Ik kan van jou wel een Justin Bieber maken maar je bent het niet en dat geldt ook voor een bedrijf. Je moet iets creëren dat bij je past, dat als jij een verhaaltje verteld dat je het zelf gelooft en daarin geloof je omdat het zo is.</code></p>

<h3>Heb je wel eens positief of negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

<p>Nee nog niet, als het terecht was had ik dat zeker gedaan. <code>Positief wel, als specialistische drukker.</code> Ik zeg ook tegen mijn klanten dat jullie het drukken.</p>

<h3>Wat zou je overhalen om vaker voor VSGM te kiezen?</h3>

<p>Niets, wellicht kan ik jullie nog gebruiken voor desktop publishing maar daar heb ik freelancers voor.</p>

<h3>Wat vind je van de huidige fysieke uitstraling?</h3>

<p><code>‘Meer dan alleen papier’? Dus eigenlijk ga je je al verontschuldigen dat je drukker bent</code> maar je plaatst het wel bewust als laatste blokje? Daarmee zeg je eigenlijk dat jullie drukker zijn. Is ‘Communicatie met visie’ jullie nieuwe slogan? Sterk in creatieve concepten. Vind je deze map getuige van communicatie met visie en sterk in creatieve concepten of is dit gewoon een mooi stukje drukwerk met een preegje? <code>Ik vind dit niet getuige van communicatie met visie en sterk in creatieve concepten, ik vind dit een mooi stukje drukwerk. Jullie roepen het wel maar je bent het niet of je straalt het in ieder geval niet uit.</code> Jullie vallen steeds terug op je drukwerk, zet die pers maar eens een maand uit en ga dan nog maar eens kijken wat er overblijft. Als er dan wat overblijft, dan is dat is wat je bent. Als dat niks is dan ben je dus een drukker. </p>

<p><code>Met je kameleon zou je conceptueel veel meer kunnen.</code></p>

<p><code>De mediatour ziet er leuk uit maar jullie redeneren alles vanuit je eigen diensten i.p.v. de vraag van de klant.</code> Ook daar zit bij jullie een addertje onder het gras maar in jullie geval zelfs een boa constrictor. Want <code>stel je weet dat allemaal mooi te verkopen en die klant komt enthousiast bij jullie langs dan... Wat is het dan, klopt dat, is dat in overeenstemming met elkaar. Als dat het geval is, is dat goed. Zo niet dan heb je een Justin Bieber van jezelf gemaakt. Door de hele bedrijfskolom moet alles kloppen.</code></p>

<p>Zoals jullie nu bekend zijn is nu is is er niks mis mee maar zoals ik je nu begrijp zou je graag anders willen zijn of worden maar op de manier waarop dat nu gebeurd heb ik daar wel mijn vraagtekens bij. Moet je dat nu met alle geweld willen of moet je gewoon eens goed kijken naar wat je nu bent, zet die pers maar een maand uit en zie welke pareltjes in je bedrijf er overblijven. Dat moet je uitvergroten. <code>Ik zie jullie als een drukkerij en jullie doen nog dingen daarbij, zo zie ik jullie en zo gedragen jullie je ook. En dat is logisch want zo ben je en daar is niets mis mee. Maar je moet geen praatjes gaan verkopen, dat zou een valkuil voor jullie kunnen zijn.</code></p>

	<?php include"../../includes/footer.php" ?>
</body>
</html>

