<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 2</li>
			</ul>
		</section>
	</header>
	<section>
<h1>Imago interview respondent 2</h1>

<h3>Waar aan denk je bij VSGM?</h3>

<p>Drukwerk, websites, direct mailing en verzorging van alle pr. <code>Als ik iets met pr te doen zou hebben zou ik eerst Mike bellen om te vragen wat hij voor me kan betekenen.</code> </p>

<h3>Welke diensten biedt VSGM aan?</h3>

<p>Drukwerk, direct mailing, websites, begeleiding in pr en promotie. Het complete plaatje en daar maken we ook gebruik van. Eerst zaten we met onze huisstijl, de folder en dergelijke bij van Tilburg maar met hen heb ik verder niets maar gezien mijn goede band met Mike heb ik langzaamaan alles bij hem geparkeerd. <code>Ik denk dat de focus met name op webdesign ligt</code>. Mike ziet in dat daar de toekomst ligt en ik denk dat hij daar zeker een goede keuze in zou maken.</p>

<h3>Wat heeft je doen besluiten om die diensten door VSGM uit te laten voeren en niet door anderen?</h3>

<p>Het is een stukje gunnen omdat ik goed met Mike om ga, <code>de gesprekken die we hebben zijn goed, onze bedrijven liggen elkaar, voort wat hoort wat.</code> Het vertrouwen is er en <code>VSGM kan goed uitleggen of verantwoorden waarom ergens voor gekozen is</code> dan heb ik snel <code>‘als jij het regelt is het wat mij betreft prima’</code>. </p>

<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

<p>Jullie werken hetzelfde als wij dus ik zou iets zeggen als <code>“als je ontzorgt wilt worden moet je bij hen wezen, leg je probleem er voor en dan wordt het geregeld. De lijnen zijn kort, de contact zijn goed. Aanradertje!&quot;</code></p>

<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

<p><code>Het is dat ik Mike ken, anders was ik nooit in aanraking gekomen met Van Schaik omdat ik ze nergens zie.</code> Misschien dat dat in Oudewater anders is maar in Reeuwijk kan ik Van Schaik roepen maar niemand die weet wat voor bedrijf dat is. Je ziet geen advertenties, sponsoring of wat dan ook. <code>Wat ze voor anderen doen doen ze eigenlijk zelf niet, ze geven wat dat betreft niet het goed voorbeeld.</code> Ik denk dat dat zeker een zwakte van ze is die ik op zou pakken, <code>meer acquisitie plegen</code>. Kijk nu ze 30 jaar bestaan krijg ik een leuke attentie, met de pasen vorig jaar eveneens. Maar dat zijn allemaal van die eenmalige dingetjes terwijl ik juist denk <code>“je moet meer de boer op om dat wat je doet te promoten”</code>. </p>

<p>Ook zou ik <code>naar mijn klanten toe beter aangeven dat je voor ‘dat’ bij moet je Patrick wezen, voor ‘dat’ moet je bij Carolien wezen, voor dat moet je bij.. etc</code>. Dat moet je <code>direct al naar buiten toe communiceren zodat de klant direct weet met de juiste om tafel te zitten</code>. En ik zou <code>minder met parttimers werken, dat is bloed irritant</code> want dan moet ik gaan bijhouden wanneer jullie er zijn.</p>

<h3>Heb je wel eens positief of negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

<p>Ik heb Van Schaik alleen maar aanbevolen. <code>Dan gaat het over het ontzorgen wat jullie goed doen</code>. Een vriend van me, die kunstenaar is, heb ik jullie ook aangeraden en ook hij is zeer tevreden over jullie. <code>Dat pakt VSGM op en dan weet je dat het goed gaat. </code></p>

<h3>Wat zou je overhalen om vaker voor VSGM te kiezen?</h3>

<p><code>Ik ben al overgehaald doordat jullie mij zo ontzorgen.</code> Ik doe nog wel eens wat drukken bij <code>een internetdrukker, dat kost me de helft minder en de levering is sneller</code>. Dat zou nog een reden zijn om me over te halen maar of jullie dat soort drukkers moeten beconcurreren betwijfel ik.</p>

<h3>Wat is kenmerkend voor VSGM?</h3>

<p><code>Dat jullie een totaalconcept maken en mij daarbij ontzorgen met jullie kennis terwijl anderen zich specialiseren op alleen websites, drukwerk, vormgeving of iets dergelijks.<br/>Dat zie ik wel als enorme meerwaarde, jullie kunnen beter anticiperen op hoe ik wat moet of wil doen.</code></p>

<p>Ik denk wel dat de <code>bedrijven die zich ergens in hebben gespecialiseerd beter zijn in dat wat ze doen dan dat jullie dat doen maar daar heb ik vrede mee, het is voor ons ook gemakzucht</code>. Ik weet wel wat ik wil maar heb geen verstand hoe, dat weten jullie mooi te beantwoorden. Jullie doelgroep is niet de Hema, Coca Cola of weet ik veel wat, die hebben hele andere wensen dan wat jullie hebben en die komen bij de specialist uit. Maar jullie doelgroep, de MKB’er uit de regio, kan prima met jullie kennis geholpen worden. </p>

<p><code>Het fijne bij Van Schaik is ook dat er iemand is zoals jij, jonge lui. Die hebben andere gedachten, die kijken ergens heel anders naar en dat moet ook met websites. Die oude pummels zijn zeker minder goed dan jonge lui zoals jij, de markt is van de jeugd.</code></p>

<h3>Wat vind je van de huidige fysieke uitstraling en dan met name de website?</h3>

<p><code>Dit is niet wouw “dat is een bedrijf die zou alle pr voor me kunnen doen”. Je moet echt gaan lezen en goed gaan kijken om te zien wat jullie doen. Het is niet dat je hier een grafisch bureau in ziet.</code> Bij de website heb ik hetzelfde gevoel. Dat komt omdat jullie te veel doen en dat kan je niet verwoorden in 1 logo of uitdrukking en dat is het probleem. Bij een timmerman is dat makkelijk, een hamer en een zaag en klaar maar bij jullie... Ik zie het zeker niet terug in de huisstijl. </p>

<p><code>De kameleon is wel leuk maar erg verstopt en misschien wel wat ouderwets.</code> Het springt er niet uit nu. De gedachte van een kameleon past bij jullie, de kameleon veranderd en jullie eveneens. Jullie passen je ook aan aan de markt. </p>

<p>Ik denk dat het heel belangrijk is hoe je binnen bent gekomen bij jullie.<code> Als je binnen bent gekomen voor drukwerk dan ga je niet zo snel ook andere werkzaamheden laten uitvoeren want het is ‘de drukker’,</code> dan ga je daarvoor verder kijken. Als jullie met nieuwe/jonge bedrijven aan de haal gaan heb je direct een betere instap want die hebben alles nodig. Als ze binnen komen voor een website dan is er veel meer contact en kom je automatisch ook met ander werk naar jullie toe. Dat is een ideaal binnenkomertje.</p>

<p><code>Ze zoeken het niet achter jullie omdat je het ten eerste niet goed gepromoot hebt en de ervaring is er niet. Het is een gedachte dat de drukker dat niet kan maar jij hebt wel bewezen dat het bij jullie niet het geval is. Maar jullie huisstijl bevestigt dat ook niet, jullie zijn daarmee niet beter dan een drukker.</code></p>

<p>Ik weet wel hoe moeilijk het is om een goed beeld neer te zetten, ikzelf heb hetzelfde probleem natuurlijk. Daarom ook ‘Thuiscomfort Groenehart’. <code>Ik zou jullie aanraden om daar door een externe partij naar te laten kijken. Die heeft misschien dezelfde kennis als jullie maar kijkt er wel op een andere manier naar.</code></p>
	</section>
	<?php include"../../includes/footer.php" ?>
</body>
</html>
