<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 4</li>
			</ul>
		</section>
	</header>
	<section>
<h1>Imago interview respondent 4</h1>

<h3>Waar aan denk je bij VSGM?</h3>

<p><code>Aan een drukker, vormgeving en websites. Die drie dingen in willekeurige volgorde. Voor mij is het meer vormgeving en websites en dan drukwerk maar jullie komen meer over als een drukker</code>. <code>Ik vind het fijn dat alles bij jullie bij elkaar zit, dat als ik het grafische deel laat maken dat het ook goed gedrukt gaat worden en niet dat ik daar dan weer tussen zit.</code> Degene die het ‘aanlevert’ weet ook dat het goed gedrukt kan worden. <code>Dat jullie alles binnen een bedrijf doen kan ook teniet doen aan de kwaliteit ergens omdat het niet tot jullie core business behoord maar volgens mij kan het in deze tijd prima allemaal onder één dak en dat bewijzen jullie ook wel tot nu toe.</code> </p>

<h3>Welke diensten biedt VSGM aan?</h3>

<p>Website, vormgeving, drukwerk. Of jullie meer doen dat dan? Geen idee! </p>

<h3>Van welke maak jij gebruik?</h3>

<p>Website, vormgeving, drukwerk.</p>

<h3>Wat heeft je doen besluiten om die diensten door VSGM uit te laten voeren en niet door anderen?</h3>

<p>De afstand hielp echt mee, dat jullie dichtbij zitten is ideaal. Maar het hielp ook echt dat <code>jullie een zelfde soort bedrijf zijn als dat wij zijn, gewoon no-nonsense</code> zoals dat hier ook is. Het is <code>dezelfde cultuur en dat praat lekker maar jullie hebben wel gewoon echt veel verstand van zaken</code>. Bij de kennismaking kwam dat wel echt duidelijk naar voren, <code>jullie weten echt waar je het over hebt</code>. Er worden geen dingen beloofd of mooie praatjes gegeven die je achteraf niet waar kan maken of uitvoeren. In het verleden ben ik wel gewend dat de wereld wordt belooft en dan duurt het langer en langer en dan kan dit toch niet of dat toch niet... <code>Jullie zijn wel heel no-nonsense met ‘dit kunnen we wel, dit kunnen we niet’</code> en dat siert jullie. Eerlijkheid en duidelijkheid is dan heel fijn, dat heeft wel bijgedragen in de hoeveelheid werk die we bij jullie hebben neergelegd. </p>

<p>Jullie waren al onze drukker. Toen zijn we op zoek gegaan naar websites in de regio die vormgeving zou kunnen doen totdat iemand er mee kwam dat jullie dat ook wel konden. Toen hebben we ook jullie site bezocht en zijn we gaan bellen voor een afspraak met het idee van ‘we zien het wel’. <code>Er waren heel veel twijfels hier intern, doe maar niet ‘dat is een drukker’. Het vertrouwen was er niet maar omdat het dichtbij was wilden we jullie toch een kans gunnen.</code> Bij dat gesprek kregen we gelukkig een heel goed gevoel, <code>jouw kennis en dat van Carolien kwam echt goed naar voren en het no-nonsense werd ook meteen duidelijk</code>. De kennis waar we aan twijfelden werd echt direct weg genomen toen we met jullie om tafel zaten, dat was echt een schot in de roos. De zorgen die wij hadden met ‘kan een partij ons helpen zonder dat we de onafhankelijkheid verliezen’ bleek ook geen issue dus we gingen met een heel goed gevoel weer weg bij jullie.</p>

<p><code>Die twijfel was wel afkomstig van ‘jullie zijn een drukkerij’ en jullie uitstraling, die komt niet over als de professional.</code> </p>

<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

<p>Als ik jullie moet beschrijven aan een collega zou ik jullie zeker aanraden. <code>Al is het ook door jullie uitstraling, jullie zitten op een industrieterrein in Oudewater en zijn een drukker. Dus dan ben je veel meer productie en dat is een ander type mens.</code> Bij marketing en communicatie, vormgeving en websites denk je al snel aan van die gladde mensen en dat zijn jullie niet. <code>What you see is what you get bij jullie en die dingen zijn ook gewoon goed</code>. Jullie denken ook echt goed mee, ook creatief en dat zou ik zeker doorgeven.</p>

<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

<p>Ik denk dat ik de tak ‘websites, concept en ontwerp’ verder zou gaan professionaliseren. <code>Dus duidelijker neer zetten naar de buitenwereld maar ook mensen die het gezicht daarvan zijn, zoals jij en Carolien, meer op pad sturen want jullie zijn oprecht goede verkopers omdat jullie er verstand van hebben.</code> Daar is in de toekomst nog veel meer te halen en jullie zouden dat verder kunnen professionaliseren. <code>Ook veel meer laten zien dat jullie dat kunnen, veel meer promoten met voorbeelden wat jullie daar al eerder hebben gedaan.</code></p>

<h3>Heb je wel eens positief of negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

<p>Negatief niet, ik ben gewoon heel tevreden zoals het nu loopt. Positief wel, <code>met name over de verbazing in de kwaliteit die we terug krijgen</code>. Negatief daaraan is natuurlijk wel dat het verbazing is en niet vanzelfsprekend maar goed. <code>Ik ken jullie pas net en niet via het drukwerk maar ik merk wel hier dat er vanuit de anderen hier veel meer zorgen over zijn.</code> Ik heb toen gezegd ‘we gaan een gesprek aan’ maar <code>steeds hoop ik wel dat het dan niet mis gaat. Maar steeds weer zijn we verbaast over de goede kwaliteit die geleverd wordt dus dat is goed.</code></p>

<h3>Wat zou je overhalen om vaker voor VSGM te kiezen?</h3>

<p>Lagere kosten natuurlijk. Maar nee verder heb ik niet echt iets, we kiezen al veel voor jullie. Misschien doen jullie dat wel al hoor maar <code>des te meer je ontzorgt in marketing, en dan met name voor het MKB zoals dit, daar zou nog een grote winst voor jullie te behalen zijn.</code> Met die diensten zou je ons maar ik denk ook anderen nog enorm kunnen helpen. Wij hebben nu net een direct mailing gemaakt zonder jullie maar dat hadden we net zo goed met jullie kunnen doen. Content op websites plaatsen, social media uit handen nemen, dat soort dingen. </p>

<h3>Wat is kenmerkend voor VSGM?</h3>

<p><code>No-nonsense, eerlijk en duidelijk doen wat je kan en niet wat je niet kan.</code></p>

<h3>Wat vind je van de huidige fysieke uitstraling en dan met name de website?</h3>

<p><code>Die kameleon vind ik echt niets. Het past misschien wel bij jullie verschillende diensten maar hij’s zo ouderwets. De manier zoals hij is opgebouwd is ook niet meer van deze tijd.</code> Die verschillende kleuren/blokjes zijn wel duidelijk maar <code>ik zie niet echt een duidelijke identiteit of vormgeving die presenteert wat je nu eigenlijk bent</code>. De kleuren hiervan zijn ook niet echt van deze tijd, die harde kleuren is niet echt van deze tijd. <code>Ik heb niet het gevoel dat ik terugzie zoals ik jullie ken in de stijl die er nu ligt.</code> Wel dat jullie de goede kant op gaan maar alsof het een klein stapje is naar te laten zien dat jullie meer zijn. <code>Het kan veel beter, meer identiteit, het blijft heel erg op de vlakte. Iets meer persoonlijkheid, dat past wel bij jullie.</code></p>

<p>Je bent een full service bureau met zoveel service dat jullie niet stoppen bij het drukwerk maar zelfs dat er bij doen en alles op de post doen. Jullie doen alles van A tot Z, A voor het mee denken en de Z voor het drukken en alles daartussen voeren jullie tevens uit. Maar <code>dat de focus ligt op concept, ontwerp en websites is niet duidelijk, dat gevoel heb ik niet</code>. <code>Je hebt mensen die goed zijn in hun werk en mensen die goed zijn in hun werk en dat ook kunnen vertellen en dat zijn jij en Carolien wel, daar heeft Mike mazzel mee en dat zou hij extra kunnen benutten. Dat maakt je een stuk geloofwaardiger dan een vertegenwoordiger op pad sturen met mooie praatjes.</code> Jullie visuele uitstraling mag dan ook echt wel een stukje flitsender; wij als bedrijf X willen wat extra’s en dan moeten jullie wel het goede voorbeeld geven. <code>Het moet flitsender zijn dan dat we zelf zijn, nog iets hipper. Pas dan wel op dat jullie niet te gladjes worden, dat is een valkuil.</code> <code>Die blokjes met daarin de woorden is ook te veel, bij het tweede kopje ben je het al kwijt en lees je het niet meer.</code> Je kan zo veel willen vertellen maar dat komt niet meer aan, <code>die blokjes weg maar dan is wel de uitdaging om jullie bedrijf goed te omschrijven.</code></p>

	<?php include"../../includes/footer.php" ?>
</body>
</html>
