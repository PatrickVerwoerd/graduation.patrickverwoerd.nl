<!DOCTYPE html>
<html>
<head>
	<?php include"../../includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"../../includes/header-title.php" ?>
		<section>
			<ul>
				<li><a href="/">Home</a></li>
				<li><a href="/fase1/">Fase 1</a></li>
				<li><a href="/fase1/imago-interviews/">Imago interviews</a></li>
				<li>Respondent 6</li>
			</ul>
		</section>
	</header>
	<section>
<h1>Imago interview respondent 6</h1>

<h3>Waar aan denk je bij VSGM?</h3>

<p><code>Dan denk ik met name aan drukwerk. Het is van vroeger uit de drukker van Oudewater</code>, samen met Heno waren dat de twee partijen waar je terecht kon voor drukwerk. <code>Aan die oorsprong denk ik al snel weer bij VSGM.</code></p>

<h3>Welke diensten biedt VSGM aan?</h3>

<p><code>Nu weet ik inmiddels, doordat ik jou mijn website heb laten maken. Dat ze naast het drukwerk ook meerdere dingen doe, dat ze de gehele communicatie oppakken. Maar dat is niet iets wat je van tevoren weet</code> want ja nogmaals de associatie met de drukkerij blijft overheersen. <code>Maar nu ik via jou te weten ben gekomen dat ze zich ook bezighouden met websites en dergelijke, met de huisstijl en wat er bij komt kijken. Er zit een studio bij die relatief groot is maar waarvan ik niet op de hoogte was.</code></p>

<h3>Van welke maak jij gebruik?</h3>

<p>Ik maak gebruik van jullie website en vormgeving diensten. </p>

<h3>Wat heeft je doen besluiten om die diensten door VSGM uit te laten voeren en niet door anderen?</h3>

<p><code>Als ik heel eerlijk ben komt dat omdat ik met jou in contact ben gekomen over de website</code>. We hebben daar voorheen al veel over gepraat waardoor ik het idee kreeg dat jij wist wat ik met de website wilde gaan doen en waardoor de keuze makkelijk was om het door jou uit te laten voeren. <code>Jij werkt bij VSGM dus vandaar dat ik bij VSGM deze diensten heb ondergebracht, dat is dus puur vanwege jou en niet vanwege het bedrijf.</code> <code>Ik moet ook heel eerlijk zeggen dat als jij daar niet zou werken ik nooit bij VSGM terecht was gekomen. Puur vanwege de associatie als drukker.</code></p>

<h3>Hoe zou je VSGM beschrijven aan een vriend?</h3>

<p><code>Als een communicatie bureau die in staat is mee te denken van A tot Z met hoe jij je merk kan neerzetten.</code> Dat is gekomen sinds ik bij jullie de website heb laten ontwikkelen, toen ben ik daar achter gekomen. Maar <code>in de regio denk ik niet dat ze jullie ook zo kennen, daar kennen ze je meer van het drukken</code>. Maar ik zie jullie tegenwoordig wel als een communicatiebureau omdat ik heb gezien dat jullie alles aanbieden. Jullie denken mee van A tot Z, het creëren van de huisstijl maar ook SEO optimalisatie. Daarbij zijn jullie ook in staat om mee te denken of te adviseren. </p>

<p><code>Ik doe ook diensten bij anderen onderbrengen, dan vooral op het gebied van marketing.</code> Enerzijds omdat ik het belangrijk vindt om er meerdere partijen bij te betrekken maar ook <code>omdat ik niet het idee heb dat jullie op dat gebied de juiste kennis in huis hebben.</code> Ze kunnen het wel maar niet op de manier zoals ik het voor ogen heb en daarom heb ik er andere partijen bij gehaald. </p>

<h3>Stel jij bent de baas van VSGM, hoe zou jij het dan aanpakken?</h3>

<p><code>Ik zou het proberen te vernieuwen. Het huidige VSGM is nog te traditioneel.</code> Ze proberen het wel maar dat komt niet helemaal door, als je het gaat vergelijken met andere ontwerpbureau’s waar allemaal jonge mensen lopen die op de hoogte zijn van de laatste technieken dan denk ik dat daar nog een gevaar voor VSGM ligt. De traditionele klanten heb je nog wel maar ik denk dat er een verandering aan zit te komen waarbij er vernieuwing nodig is. <code>Met name heb ik het gevoel dat er jongere mensen aangenomen moeten worden om die hippere uitstraling te krijgen.</code> Dan ga je automatisch ook hipper werk leveren dan het huidige traditionele en conservatieve werk dat er nu wordt geleverd. Ik heb wel het idee dat als er jongere mensen zouden werken er ook jonger werk wordt geleverd. I<code>k heb het gevoel dat het nu een beetje ontbreekt aan creativiteit, dat het wat in een sleur zit. Ik zeg niet dat het zo is maar dat gevoel krijg ik bij jullie, als ik dan naar jullie studio kijk dan zitten er veel dezelfde typen mensen maar jij bent daartussen duidelijk een uitzondering.</code> Als ik de baas van VSGM zou zijn zou ik <code>meer behoefte hebben aan jongen mensen die verstand hebben van social media en de huidige tijd. Dat heeft VSGM nodig voor de toekomst en al de veranderingen die er zijn.</code></p>

<h3>Heb je wel eens positief of negatief over VSGM gesproken tegen een vriend/collega of wie dan ook? Waar ging dat over?</h3>

<p><code>Ik heb wel eens negatief gesproken. Dat ging toen voornamelijk over de communicatie bij het opleveren van de website.</code> Dat kwam toen door meerdere factoren waardoor ik het VSGM zelf niet kwalijk kan nemen. <code>Maar daarbij liep ik wel aan tegen een stukje communicatie, afspraken werden niet goed gemaakt.</code> Daar heb ik me toen ook negatief over geuit naar anderen toe. <code>Maar dat wat er inmiddels is opgeleverd ben ik wel heel blij mee, jullie leveren echt goed werk.</code></p>

<p>Positief heb ik ook zeker over jullie gesproken. De website die jullie voor mij gemaakt hebben ben ik echt zo blij mee, daar ben ik super tevreden over. Daar hoor ik iedereen over, die is gewoon heel mooi en goed geworden.</p>

<h3>Wat zou je overhalen om vaker voor VSGM te kiezen?</h3>

<p>Dat is lastig voor mij te zeggen, ik ben geen herhalende klant die bij jullie terugkomt voor bijvoorbeeld een nieuwe campagne. Dit is direct ook wel een punt; <code>als jullie zouden meedenken over de marketing dat ik daar geen ander voor nodig heb. Dat jullie een breder totaalpakket kunnen aanbieden waarbij wordt meegedacht over heel de communicatie en marketing. Ik heb nu niet het gevoel dat het nu goed mogelijk is bij jullie.</code></p>

<p>Wat betreft prijzen zijn jullie gewoon marktconform, ik kan wel zeggen de prijs maar ergens anders kan het niet goedkoper met dezelfde kwaliteit. </p>

<h3>Wat is kenmerkend voor VSGM?</h3>

<p>Ik durf wel te zeggen dat, en dan ga ik toch weer naar de drukwerk kant, dat jullie daar wel echt heel goed in zijn en dat helemaal op orde hebben. Daar hebben jullie echt kwaliteit. Zelf doe ik het drukwerk via drukwerkdeal en als ik dat dan laat zien dan merk jij wel terecht op wat er allemaal niet goed aan is. Maar goed, dat maakt me voor dat werk weinig uit. Dat is prijs &gt; kwaliteit. </p>

<p>Maar goed, kenmerkend voor VSGM. <code>Dan denk ik wel direct aan het drukwerk gedeelte en niet zozeer aan de studio kant.</code> Dat komt voornamelijk <code>doordat het vroeger ‘Van Schaik Offset’ hete. Nu heet het ‘Van Schaik Grafimedia hè’? Dat offset gebeuren zit er nog helemaal ingeslepen, dat imago blijft er toch.</code> </p>

<p>Ook <code>als ik bij jullie binnenkom blijft het wel een drukkerij</code>, dat heeft gewoon de overhand en daar ligt de core business. Jullie willen ‘stiekem’ heel graag richting de marketing en communicatie kant maar dat vinden jullie nog erg lastig. En nogmaals, <code>de mensen die er nu zitten, daarbij heb ik niet het gevoel dat die dat vernieuwende gaan doorvoeren. Dat Van Schaik met hen het totaalconcept kan gaan leveren. Investeren in jong talent is zeker nodig, nu werken er naar mijn mening veel mensen die een beetje vastgeroest zitten in hun functie en heel erg ervaren zijn maar niet buiten hun boxje denken.</code> Het lijkt mij voor Van Schaik heel waardevol om jonge mensen in huis te halen die bijvoorbeeld ervaring hebben in de marketing. Niet de grafische kennis per se maar de marketingkennis, die moeten elkaar dan gaan aanvullen. </p>

<h3>Wat vind je van de huidige fysieke uitstraling en dan met name de website?</h3>

<p><code>Als ik deze website zo zie krijg ik wel het idee dat jullie de goede kant op gaan. Dat het er wel hipper en vernieuwender uitziet.</code> Hierbij krijg je wel meer het marketing bureau idee in plaats van een drukkerij. </p>

<p>Het is wel vernieuwd met dat 3d geprinte werk, hoe noem je dat? Dat is wel echt mooi, ziet er gaaf uit. Maar qua stijl ja... Het is met die blokjes duidelijk omschreven wat je nu doet maar... Maar <code>het logo vind ik echt helemaal niks, daarbij krijg ik echt weer de associatie met het vroegere drukwerk gedeelte. Het oogt ouderwets moet ik ook zeggen. Dat kan veel hipper en leuker.</code> Want hoe moet ik jullie logo zien, is dat kameleonetje daar altijd mee verbonden of hoort dat ‘Van Schaik Grafimedia » VSGM’ er ook bij? <code>Bij een beeldmerk moet proberen dat je gelijk een beeld krijgt bij wat een bedrijf doet maar als ik dit zo zie dan heb ik dat helemaal niet. Ik heb er de associatie mee dat het Van Schaik Offset is, dat doet mijn gedachtes aan de drukkerij allemaal maar bevestigen.</code><code> Het past ook niet bij de 5 blokjes, die zien er vernieuwd uit maar dan staat er zo’n oud logo naast. Die 5 blokjes zijn wel leuk vind ik eigenlijk.</code> </p>

<p><code>Die letters op jullie briefpapier ja, weet niet wat ik daar van moet vinden. Daarbij heb ik echt ‘what the fack is dit nou weer’?</code> Die folie ziet er strak uit, dat kost wel wat zeker? Wij krijgen de factuur ook op deze manier, dat is wel netjes. </p>

<p><code>Bij de slogan mis ik wel een vaste slogan. Ik zie hier staan ‘Van Schaik Grafimedia, meer dan alleen papier’</code> en dat is echt de meest vreselijke slogan ooit. <code>Dat klinkt zo simpel, ik mag hopen ja dat het meer dan alleen papier is. En hier zie ik weer ‘communicatie met visie’, ‘sterk in creatieve concepten’ staan.</code> Dat laat wel weer zien dat jullie die kant op willen. <code>Maar terecht is het niet dat op de map van VSGM staat om eerlijk te zijn.</code> <code>Als je binnen komt lopen bij jullie kom je voor je gevoel toch echt een drukkerij binnenlopen. Ik heb dan niet het gevoel dat je bij een creatief bureau binnenloopt die met je mee denkt van a tot z, wat jullie wél zouden willen natuurlijk.</code> Dus als ik hele kritisch moet zijn moet ik zeggen dat <code>het niet aansluit op dat wat jullie willen, dat wat jullie doen is nog niet echt ‘communicatie met visie’.</code> </p>

	<?php include"../../includes/footer.php" ?>
</body>
</html>

