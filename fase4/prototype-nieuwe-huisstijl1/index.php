<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Huidig design, prototype v1</title>
	<link rel="stylesheet" type="text/css" href="fullpage.css" />

	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

	<script type="text/javascript" src="scrolloverflow.js"></script>
	<script type="text/javascript" src="jquery.fullPage.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				anchors: ['eerste', 'tweede', 'derde', 'vierde', 'vijfde'],
				navigation: true,
				navigationPosition: 'right',
				// navigationTooltips: ['Wi', 'tweede', 'derde', 'vierde', 'vijfde']
			});
		});
	</script>

</head>
<body>
	<header>
	</header>
	<div id="fullpage">
		<div class="section" id="section1">
		</div>
		<div class="section" id="section2">
		</div>
		<div class="section" id="section3">
		</div>
		<div class="section" id="section4">
		</div>
		<div class="section" id="section5">
		</div>
	</div>
</body>
</html>