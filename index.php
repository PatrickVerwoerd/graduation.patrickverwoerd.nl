<!doctype html>
<html lang="nl">
<head>
	<title>Graduation appendix || Patrick Verwoerd</title>
	<?php include"includes/header.php" ?>
</head>
<body>
	<header>
		<?php include"includes/header-title.php" ?>
		<section>
			<ul>
				<li>Home</li>
			</ul>
		</section>
	</header>
	<section class="home">
		<div class="contents">
			<h2>Fase 1: Empathize</h2>
			<ul>
				<li><a href="/fase1/imago-interviews/" class="folder">1.1 Transcripten imago interviews</a>
					<ul>
						<li><a href="/fase1/imago-interviews/respondent1.php">1.1.1 Transscript respondent 1</a></li>
						<li><a href="/fase1/imago-interviews/respondent2.php">1.1.2 Transscript respondent 2</a></li>
						<li><a href="/fase1/imago-interviews/respondent3.php">1.1.3 Transscript respondent 3</a></li>
						<li><a href="/fase1/imago-interviews/respondent4.php">1.1.4 Transscript respondent 4</a></li>
						<li><a href="/fase1/imago-interviews/respondent5.php">1.1.5 Transscript respondent 5</a></li>
						<li><a href="/fase1/imago-interviews/respondent6.php">1.1.6 Transscript respondent 6</a></li>
						<li><a href="/fase1/imago-interviews/respondent7.php">1.1.7 Transscript respondent 7</a></li>
					</ul>
				</li>
				<li><a href="/fase1/GewensteIdentiteit.pdf" target="_blank">1.2 Gewenste identiteit</a></li>
				<li><a href="/fase1/VisueleIdentiteit.pdf" target="_blank">1.3 Visuele identiteit</a></li>
				<li><a href="/fase1/Doelgroep.pdf" target="_blank">1.4 Doelgroep omschrijving eerder onderzoek</a></li>
				<li><a href="/fase1/Archetypes.pdf" target="_blank">1.5 Archetype posters</a></li>
			</ul>
		</div>
		<div class="contents">
			<h2>Fase 2: Define</h2>
			<ul>
				<li><a href="" class="todo">2.1 Voorbeeld service bleuprint</a></li>
				<li><a href="" class="todo">2.2 Competitive testing</a>
					<ul>
						<li><a href="" class="todo">2.2.1 Concullega Reprovinci</a></li>
						<li><a href="" class="todo">2.2.2 Concullega TamTam</a></li>
						<li><a href="" class="todo">2.2.3 Concullega Van Der Perk Groep </a></li>
						<li><a href="" class="todo">2.2.4 Concullega De Toekomst</a></li>
						<li><a href="" class="todo">2.2.5 Concullega Drukwerkdeal</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="contents">
			<h2>Fase 3: Ideate</h2>
			<ul>
				<li><a href="" class="todo">3.1 Mindmaps</a></li>
				<li><a href="" class="todo">3.2 Layout schetsen</a></li>
				<li><a href="" class="todo">3.3 Grove concept schetsen</a>
					<ul>
						<li><a href="" class="todo">3.3.1.1 Concept 1.1</a></li>
						<li><a href="" class="todo">3.3.1.2 Concept 1.2</a></li>
						<li><a href="" class="todo">3.3.1.3 Concept 1.3</a></li>
						<li><a href="" class="todo">3.3.2 Concept 2</a></li>
						<li><a href="" class="todo">3.3.3 Concept 3</a></li>
						<li><a href="" class="todo">3.3.4 Concept 4</a></li>
						<li><a href="" class="todo">3.3.5 Concept 5</a></li>
					</ul>
				</li>
				<li><a href="" class="todo">3.4 Conceptschets v2 frontpage</a></li>
				<li><a href="" class="todo">3.5 Conceptschets v2 casestudie</a></li>
				<li><a href="" class="todo">3.6 Conceptschets v3 frontpage</a></li>
				<li><a href="" class="todo">3.7 Conceptschets v3 casestudie</a></li>

				<li><a href="" class="todo">3.8 Cardsorting sessie 1</a></li>
				<li><a href="" class="todo">3.9 Cardsorting sessie 2</a></li>
				<li><a href="" class="todo">3.10 Cardsorting sessie 3</a></li>
				<li><a href="" class="todo">3.11 Low fidelity Wireframe v1</a></li>
			</ul>
		</div>

		<div class="contents">
			<h2>Fase 4: Prototype &amp; Validate</h2>
			<ul>
				<li><a href="" class="todo">4.1 Uitwerkingen wireframe</a>
					<ul>
						<li><a href="" class="todo">4.1.1 Navigatie</a></li>
						<li><a href="" class="todo">4.1.2 Header concept</a></li>
						<li><a href="" class="todo">4.1.3 Case studies</a></li>
						<li><a href="" class="todo">4.1.4 Keuze wireframe</a></li>
					</ul>
				</li>
				<li><a href="" class="todo">4.2 Schetsen logo</a></li>
				<li><a href="" class="todo">4.3 Digitale schetsen logo collega</a></li>     
				<li><a href="" class="todo">4.4 Uitwerkingen logo v1</a></li>     
			</ul>
		</div>
	</section>
	<!-- <section>
		<div class="slider">
			<div>
				<img src="http://placehold.it/1500x800&text=1" alt="">
				<h1>Concept 1</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce consectetur dolor eu diam dapibus, vel ultricies augue semper. Vivamus enim metus, mollis in purus non, euismod pellentesque urna. In iaculis quis dolor a sagittis. Pellentesque et augue vestibulum, dictum ex id, malesuada turpis. Cras sapien arcu, dapibus suscipit sem eu, iaculis dignissim arcu. Etiam scelerisque, mauris interdum auctor pulvinar, quam elit condimentum dolor, a tristique turpis lacus vitae augue. Donec non turpis sed nunc aliquet egestas. Praesent venenatis sodales maximus. Donec ac nibh sapien. Nam pellentesque venenatis aliquet. Proin id mauris ante. Cras maximus, lacus et euismod imperdiet, elit leo placerat quam, vel ornare felis enim in nunc.</p>
			</div>
			<div><img src="http://placehold.it/400x100&text=2" alt=""></div>
			<div><img src="http://placehold.it/400x100&text=3" alt=""></div>
			<div><img src="http://placehold.it/400x100&text=4" alt=""></div>
			<div><img src="http://placehold.it/400x100&text=5" alt=""></div>
			<div><img src="http://placehold.it/400x100&text=6" alt=""></div>
			<div><img src="http://placehold.it/400x100&text=7" alt=""></div>
			<div><img src="http://placehold.it/400x100&text=8" alt=""></div>
		</div>
	</section> -->

	<?php include"includes/footer.php" ?>
</body>
</html>
