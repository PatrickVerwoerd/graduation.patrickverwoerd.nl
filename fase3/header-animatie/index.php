<!DOCTYPE html>
<html >
<head>
	<title>Header slideshow home</title>
	<link rel="stylesheet" href="assets/styles/app.css" type="text/css">
</head>
<body>
	<div class="wrapper">
		<div class="section">
			<h1>Wij zijn</h1>
			<h1><a href="" class="typewrite" data-period="1000" data-type='[ "Ontwerpers", "Ontwikkelaars", "Drukkers", "Wordpress experts", "net even anders" ]'><span class="wrap"></span></a>
			</h1>
		</div>
	</div>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="assets/js/simsalabim.js" type="text/javascript"></script>
</body>
</html>